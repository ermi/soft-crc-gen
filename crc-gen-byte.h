//  soft-crc-gen: CRC code generator
//  Copyright (C) 2022  ermi
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef crc_gen_byte_h
#define crc_gen_byte_h
#include "crc-gen-base.h"

void fprint_function_byte_calc(FILE *const stream, struct generation_settings_t const *const settings, bool const mask, char const *const suffix);

void soft_crc_gen_byte_h(FILE *const stream, struct generation_settings_t const *const settings, char const *const basename);
void soft_crc_gen_byte_c(FILE *const stream, struct generation_settings_t const *const settings, char const *const basename);

#endif

