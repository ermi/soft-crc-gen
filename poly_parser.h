//  soft-crc-gen: CRC code generator
//  Copyright (C) 2022  ermi
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef poly_parser_h
#define poly_parser_h
#include <stddef.h>
#include <stdint.h>

#define POLY_PARSER_INVALID_TOKEN -1
#define POLY_PARSER_INT_OVERFLOW  -2
#define POLY_PARSER_WRONG_ORDER   -3
#define POLY_PARSER_DOUBLED_EXP   -4

int8_t poly_parse_str(size_t *const pos, uint64_t *const poly, uint8_t *const width, char const *const str);

#endif
