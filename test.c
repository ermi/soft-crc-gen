//  soft-crc-gen: CRC code generator
//  Copyright (C) 2022  ermi
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include INCLUDE_HEADER
#include "crc-utils.h"
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <inttypes.h>
#include <time.h>

#if LSB_FIRST
#define END_MASK ((uint8_t) (0xff>>(8-POLY_WIDTH%8)))
#else
#define END_MASK ((uint8_t) (0xff<<(8-POLY_WIDTH%8)))
#endif


ssize_t readstream(FILE *const stream, uint8_t **const data)
{
	size_t size = 0;
	*data = NULL;
	
	while(1)
	{
		uint8_t *const tmp = realloc(*data, size+1024);
		if(!tmp)
		{
			perror("realloc");
			free(*data);
			*data = NULL;
			return -1;
		}
		*data = tmp;
		
		size += fread(*data+size, 1, 1024, stream);
		
		if(feof(stream))
			return size;
		if(ferror(stream))
		{
			fputs("fread: error\n", stderr);
			return -1;
		}
	}
}


int test(uint8_t const *const msg, size_t const size)
{
	int err = 0;
	CRC_TYPE crc   = SEED_NAME;
	uint64_t crc64 = SEED_NAME;
	uint8_t *const data   = malloc(size+(POLY_WIDTH+7)/8);
	uint8_t *const data64 = malloc(size+(POLY_WIDTH+7)/8);
	if(!(data && data64))
	{
		free(data);
		free(data64);
		perror("malloc");
		return -1;
	}
	memcpy(data  , msg, size);
	memcpy(data64, msg, size);
	
#ifdef BITS
	FUNC_NAME_CALC(&crc, data, 0, 8*size);
#else
	FUNC_NAME_CALC(&crc, data, size);
#endif
	crc_general(&crc64, POLY, POLY_WIDTH, data64, 0, 8*size, LSB_FIRST, RSHIFT);
	if(crc!=crc64) err |= 1;
	
	printf("crc\t%" PRI_CRC "\t%" PRI_CRC64 "\n", crc, crc64);
	
	CRC_TYPE const bak_crc = crc;
	uint64_t const bak_crc64 = crc64;
	
	srand(time(NULL));
	size_t const errbit = rand()%(8*size);
	
	for(uint8_t error=0; error<2; error++)
	{
		for(uint8_t invert=0; invert<2; invert++)
		{
			crc   = SEED_NAME;
			crc64 = SEED_NAME;
			
			memcpy(data  , msg, size);
			memcpy(data64, msg, size);
#ifdef BITS
			FUNC_NAME_APPEND(invert?~bak_crc:bak_crc, data+size, 0);
#else
			FUNC_NAME_APPEND(invert?~bak_crc:bak_crc, data+size);
#endif
			crc_general_append(data64+size, 0, invert?~bak_crc64:bak_crc64, POLY_WIDTH, LSB_FIRST, RSHIFT);
			if(memcmp(data+size, data64+size, POLY_WIDTH/8)) err |= 2;
			if(POLY_WIDTH%8)
			{
				if((data[size+POLY_WIDTH/8]^data64[size+POLY_WIDTH/8])&END_MASK) err |= 2;
			}
			
			for(uint8_t i=0; i<(POLY_WIDTH+7)/8; i++)
			{
#if POLY_WIDTH<8
				uint8_t const mask = END_MASK;
#else
				uint8_t const mask = (i<POLY_WIDTH/8)?0xff:END_MASK;
#endif
				printf("byte%" PRIu8 "\t%02x\t%02x\n", i, data[size+i]&mask, data64[size+i]&mask);
			}
			
			if(error)
			{
				uint8_t const bitmask = 1<<(errbit%8);
				size_t const bitindex = errbit/8;
				data  [bitindex] ^= bitmask;
				data64[bitindex] ^= bitmask;
			}
			
#if defined BITS || POLY_WIDTH%8==0
#ifdef BITS
			FUNC_NAME_CALC(&crc, data, 0, 8*size+POLY_WIDTH);
#else
			FUNC_NAME_CALC(&crc, data, size+POLY_WIDTH/8);
#endif
			crc_general(&crc64, POLY, POLY_WIDTH, data64, 0, 8*size+POLY_WIDTH, LSB_FIRST, RSHIFT);
			
			if(crc!=crc64) err |= 1;
			if(error)
			{
				if(invert) { if(crc==MAGIC_NAME) err |= 1; }
				else       { if(crc==0         ) err |= 1; }
			}
			else
			{
				if(invert) { if(crc!=MAGIC_NAME) err |= 1; }
				else       { if(crc!=0         ) err |= 1; }
			}
			
			printf("%s_%c\t%" PRI_CRC "\t%" PRI_CRC64 "\n", error?"err":"crc", invert?'i':'n', crc, crc64);
#else // defined BITS || POLY_WIDTH%8==0
#ifdef BITS
			FUNC_NAME_CALC(&crc, data, 0, 8*size);
			uint8_t const cmp = FUNC_NAME_COMPARE(invert?~crc:crc, data+size, 0);
#else
			FUNC_NAME_CALC(&crc, data, size);
			uint8_t const cmp = FUNC_NAME_COMPARE(invert?~crc:crc, data+size);
#endif
			crc_general(&crc64, POLY, POLY_WIDTH, data64, 0, 8*size, LSB_FIRST, RSHIFT);
			uint8_t const cmp64 = crc_general_compare(data64+size, 0, invert?~crc64:crc64, POLY_WIDTH, LSB_FIRST, RSHIFT);
			
			if(error) { if(!cmp || !cmp64) err |= 1; }
			else      { if( cmp ||  cmp64) err |= 1; }
			
			printf("%s_%c\t%" PRIu8 "\t%" PRIu8 "\n", error?"err":"cmp", invert?'i':'n', cmp, cmp64);
#endif // defined BITS || POLY_WIDTH%8==0
		}
	}
	
#if defined BITS || POLY_WIDTH%8==0
	printf("magic\t%" PRI_CRC "\n", MAGIC_NAME);
#endif
	
	return err;
}


int test_string(char const *const str)
{
	return test((uint8_t*) str, strlen(str));
}

int test_stream(FILE *const input)
{
	uint8_t *data = NULL;
	ssize_t const size = readstream(input, &data);
	if(size<0) return -1;
	
	int const ret = test(data, size);
	free(data);
	return ret;
}

int test_file(char const *const fname)
{
	FILE *const input = fopen(fname, "r");
	if(!input)
	{
		perror("fopen");
		return -1;
	}
	
	int const ret = test_stream(input);
	
	if(fclose(input)==-1)
	{
		perror("fclose");
		return -1;
	}
	
	return ret;
}


/********/
/* main */
/********/
int main(int argc, char **argv)
{
	if(argc>1)
	{
		int (*test_func)(char const *const fname);
		
		if(!(strcmp(argv[1], "a") && strcmp(argv[1], "args")))
			test_func = test_string;
		else if(!(strcmp(argv[1], "f") && strcmp(argv[1], "file")))
			test_func = test_file;
		else
		{
			fprintf(stderr, "Error: unknown source type (%s)\n", argv[1]);
			return -1;
		}
		
		for(int i=2; i<argc; i++)
		{
			int const ret = test_func(argv[i]);
			if(ret) return ret;
		}
		
		return 0;
	}
	else
		return test_stream(stdin);
}

