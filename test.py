#!/usr/bin/python3

#  soft-crc-gen: CRC code generator
#  Copyright (C) 2022  ermi
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

import subprocess
import math
import os
try:
	import crcmod
except ModuleNotFoundError:
	import sys
	print('Warning: module crcmod not available', file=sys.stderr)
script_dir = os.path.relpath(os.path.dirname(__file__))
build_dir = os.path.relpath(os.path.join(script_dir, 'build'))

def reverse_bits(x, width):
	y = 0
	for i in range(width):
		y = (y<<1)|(x&1)
		x >>= 1
	return y

def type_bytes(x):
	return math.pow(2, math.ceil(math.log(x, 2)))
def type_bits(x):
	return type_bytes(math.ceil(x/8.0))*8

def val2hex(x, width):
	fmt = '%%0%dX' %((width+3)/4)
	x &= int('0b'+'1'*width, 2)
	return fmt % x

def get_seed(cnf):
	seed = cnf.get('seed', 0)
	if type(seed)==str:
		if seed=='zeros':
			seed = 0
		elif seed=='ones':
			seed = int('0b'+'1'*cnf['poly_width'], 2)
		else:
			seed = int(seed, 16)
	return seed

def generate_name(cnf):
	name = '%d'% cnf['poly_width']
	name += '_'+val2hex(cnf['poly'], cnf['poly_width'])
	name += '_'+val2hex(get_seed(cnf), cnf['poly_width'])
	name += '_lsb' if cnf.get('lsb_first', False) else '_msb'
	name += '_rshift' if cnf.get('rshift', False) else '_lshift'
	name += '_bits' if cnf.get('bits', False) else '_bytes'
	if cnf.get('use_lut', False): name += '_lut'
	return name

def run(args, **kwargs):
#	print('>', *args)
	return subprocess.run(args, **kwargs)

def generate_crc(cnf, path=build_dir):
	args = [os.path.join(build_dir, 'soft-crc-gen')]
	name = 'crc%d'% cnf['poly_width']
	
	args.append(val2hex(cnf['poly'], cnf['poly_width'])+'/'+str(cnf['poly_width']))
	args += ['-s', val2hex(get_seed(cnf), cnf['poly_width'])]
	if cnf.get('lsb_first', False): args.append('--lsb_first')
	if cnf.get('rshift'   , False): args.append('--right')
	if cnf.get('use_lut'  , False): args.append('--table')
	if cnf.get('bits'     , False): args.append('--bits')
	args += ['-n', name]
	args.append(os.path.join(path, name+'.h'))
	args.append(os.path.join(path, name+'.c'))
	
	proc = run(args, stdout=subprocess.DEVNULL)
	proc.check_returncode()

def generate_prg(cnf, path=build_dir):
	flag = ['-Wall', '-Wextra', '-Wpedantic', '-Wshadow']
	flag.append('-fpie')
	flag.append('-fstack-protector-strong')
	flag.append('-O2')
	flag.append('-I'+os.path.join('.', path))
	flag.append('-DCRC_TYPE=uint%d_t'% type_bits(cnf['poly_width']))
	flag.append('-DPRI_CRC="0%d" PRIX%d'%((cnf['poly_width']+3)//4, type_bits(cnf['poly_width'])))
	flag.append('-DPRI_CRC64="0%d" PRIX64'%((cnf['poly_width']+3)//4))
	flag.append('-DPOLY=0x%X'% cnf['poly'])
	flag.append('-DPOLY_WIDTH=%d'% cnf['poly_width'])
	flag.append('-DLSB_FIRST=%d'% cnf.get('lsb_first', False))
	flag.append('-DRSHIFT=%d'% cnf.get('rshift', False))
	flag.append('-DINCLUDE_HEADER="crc%d.h"'% cnf['poly_width'])
	flag.append('-DFUNC_NAME_CALC=crc%d_calc'% cnf['poly_width'])
	flag.append('-DFUNC_NAME_APPEND=crc%d_append'% cnf['poly_width'])
	flag.append('-DFUNC_NAME_COMPARE=crc%d_compare'% cnf['poly_width'])
	flag.append('-DSEED_NAME=CRC%d_SEED'% cnf['poly_width'])
	flag.append('-DMAGIC_NAME=CRC%d_MAGIC'% cnf['poly_width'])
	if cnf.get('bits', False): flag.append('-DBITS')
	
	dst = os.path.join(path, 'crc-test')
	src = [os.path.join(script_dir, 'test.c')]
	src.append(os.path.join(path, 'crc%d.c'% cnf['poly_width']))
	src.append(os.path.join(build_dir, 'crc-utils.o'))
	
	proc = run(['gcc']+src+['-o', dst]+flag)
	proc.check_returncode()

def python_crc(cnf, msg):
	try:
		crc_func = crcmod.mkCrcFun((1<<cnf['poly_width'])|cnf['poly'], initCrc=get_seed(cnf), rev=False)
	except NameError:
		return None
	except ValueError:
		return None
	if cnf.get('lsb_first', False):
		crc = crc_func(bytes([reverse_bits(b, 8) for b in msg]))
	else:
		crc = crc_func(msg)
	if cnf.get('rshift', False):
		crc = reverse_bits(crc, cnf['poly_width'])
	return {'crc':crc}

def test_crc(cnf, msg, path):
	args = [os.path.join(path, 'crc-test')]
	proc = run(args, input=msg, stdout=subprocess.PIPE)
	if proc.returncode<0: proc.check_returncode()
	ret = {'returncode':proc.returncode, 'stdout':proc.stdout.decode()}
	lines = ret['stdout'].split('\n')
	for line in lines:
		row = line.split('\t')
		if len(row)<2: continue
		ret[row[0]] = int(row[1], 16)
	return ret

def do_test(cnf, msg, path=build_dir):
	pyret = python_crc(cnf, msg)
	ret = test_crc(cnf, msg, path)
	
	if ret['returncode']>0 or (pyret!=None and pyret['crc']!=ret['crc']):
		print('>>>', {1:'crc', 2:'append', 3:'crc & append'}.get(ret['returncode'], 'unknown'), 'error')
		if pyret!=None: print('py\t'+val2hex(pyret['crc'], cnf['poly_width']))
		print(ret['stdout'], end='')
	else:
		print('>>> success')

def do_all(cnf, msg):
	name = generate_name(cnf)
	path = os.path.join(build_dir, name)
	try: os.mkdir(path)
	except FileExistsError: pass
	print('#', name)
	
	generate_crc(cnf, path)
	generate_prg(cnf, path)
	do_test(cnf, msg, path)


eth_head = 6*b'\xff'+b'\x02'+5*b'\0'
eth_crc = b'\x73\xc7\x8b\x41'
eth_pkg = eth_head+(60-len(eth_head))*b'\0'
eth_all = eth_pkg+eth_crc
msg = eth_pkg

poly_eth = (1<<32)|(1<<26)|(1<<23)|(1<<22)|(1<<16)|(1<<12)|(1<<11)|(1<<10)|(1<<8)|(1<<7)|(1<<5)|(1<<4)|(1<<2)|(1<<1)|(1<<0)
cnf_eth = {'poly':poly_eth, 'poly_width':32, 'seed':'ones'}

poly_24 = (1<<24)|(1<<23)|(1<<18)|(1<<17)|(1<<14)|(1<<11)|(1<<10)|(1<<7)|(1<<6)|(1<<5)|(1<<4)|(1<<3)|(1<<1)|(1<<0)
cnf_24 = {'poly':poly_24, 'poly_width':24, 'seed':'ones'}

poly_12 = (1<<12)|(1<<11)|(1<<3)|(1<<2)|(1<<1)|(1<<0)
cnf_12 = {'poly':poly_12, 'poly_width':12, 'seed':'ones'}

poly_j1850 = (1<<8)|(1<<4)|(1<<3)|(1<<2)|(1<<0)
cnf_j1850 = {'poly':poly_j1850, 'poly_width':8, 'seed':'ones'}

poly_usb = (1<<5)|(1<<2)|(1<<0)
cnf_usb = {'poly':poly_usb, 'poly_width':5, 'seed':'ones'}

poly_sd = (1<<7)|(1<<3)|(1<<0)
cnf_sd = {'poly':poly_sd, 'poly_width':7, 'seed':'ones'}

#cnf['lsb_first'] = True
#cnf['rshift'] = True
#cnf['use_lut'] = True
#do_all(cnf, msg)

cnf_all = []
cnf_all.append(cnf_eth)
cnf_all.append(cnf_24)
cnf_all.append(cnf_12)
cnf_all.append(cnf_j1850)
cnf_all.append(cnf_sd)
cnf_all.append(cnf_usb)

for cnf in cnf_all:
	for cnf['bits'] in (False, True):
		for cnf['use_lut'] in (False,) if cnf['poly_width']<8 else (False, True):
			for cnf['rshift'] in (False, True):
				for cnf['lsb_first'] in (False, True):
					do_all(cnf, msg)

