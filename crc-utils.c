//  soft-crc-gen: CRC code generator
//  Copyright (C) 2022  ermi
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "crc-utils.h"
#include <stdlib.h>
#include <string.h>

static inline uint8_t reverse8(uint8_t x)
{
	x = ((x<<1)&0xaa)|((x>>1)&0x55);
	x = ((x<<2)&0xcc)|((x>>2)&0x33);
	return (x<<4)|(x>>4);
}
static inline uint16_t reverse16(uint16_t x)
{
	return ((uint16_t) reverse8(x)<<8)|reverse8(x>>8);
}
static inline uint32_t reverse32(uint32_t x)
{
	return ((uint32_t) reverse16(x)<<16)|reverse16(x>>16);
}
static inline uint64_t reverse64(uint64_t x)
{
	return ((uint64_t) reverse32(x)<<32)|reverse32(x>>32);
}

uint64_t reverse_bits(uint64_t const x, uint8_t const width)
{
	switch((width+7)/8)
	{
	case 1:
		return reverse8(x)>>(8-width);
	case 2:
		return reverse16(x)>>(16-width);
	case 3:
	case 4:
		return reverse32(x)>>(32-width);
	case 5:
	case 6:
	case 7:
	case 8:
		return reverse64(x)>>(64-width);
	default:
		return 0;
	}
}


uint64_t generate_mask(uint8_t const width)
{
	return ~((~(uint64_t) 0)<<width);
}


static inline void crc_lshift_byte(uint64_t *const crc, uint64_t const poly, uint8_t const poly_width, uint8_t const *const data, uint64_t const data_width, bool const lsb_first)
{
	uint64_t const upper_mask = (uint64_t) 1<<(poly_width-1);
	
	for(uint64_t i=0; i<data_width; i++)
	{
		*crc ^= ((uint64_t) (lsb_first?reverse8(data[i]):data[i]))<<(poly_width-8);
		for(uint8_t n=8; n; n--)
		{
			bool const upper_bit = *crc&upper_mask;
			*crc <<= 1;
			if(upper_bit) *crc ^= poly;
		}
	}
}

static inline void crc_rshift_byte(uint64_t *const crc, uint64_t const poly, uint8_t const poly_width, uint8_t const *const data, uint64_t const data_width, bool const lsb_first)
{
	(void) poly_width;
	
	for(uint64_t i=0; i<data_width; i++)
	{
		*crc ^= lsb_first?data[i]:reverse8(data[i]);
		for(uint8_t n=8; n; n--)
		{
			bool const lower_bit = *crc&1;
			*crc >>= 1;
			if(lower_bit) *crc ^= poly;
		}
	}
}


static inline void crc_lshift_bit(uint64_t *const crc, uint64_t const poly, uint8_t const poly_width, uint8_t const *const data, uint8_t const offset, uint64_t const data_width, bool const lsb_first)
{
	char const *const bitmask = lsb_first?"\x01\x02\x04\x08\x10\x20\x40\x80":"\x80\x40\x20\x10\x08\x04\x02\x01";
	uint64_t const upper_mask = (uint64_t) 1<<(poly_width-1);
	uint64_t const subsequent = offset+data_width;
	
	for(uint64_t i=offset; i<subsequent; i++)
	{
		bool const bit = data[i/8]&bitmask[i%8];
		bool const upper_bit = *crc&upper_mask;
		*crc <<= 1;
		if(upper_bit!=bit) *crc = (*crc^poly)|1;
	}
}

static inline void crc_rshift_bit(uint64_t *const crc, uint64_t const poly, uint8_t const poly_width, uint8_t const *const data, uint8_t const offset, uint64_t const data_width, bool const lsb_first)
{
	char const *const bitmask = lsb_first?"\x01\x02\x04\x08\x10\x20\x40\x80":"\x80\x40\x20\x10\x08\x04\x02\x01";
	uint64_t const upper_mask = (uint64_t) 1<<(poly_width-1);
	uint64_t const subsequent = offset+data_width;
	
	for(uint64_t i=offset; i<subsequent; i++)
	{
		bool const bit = data[i/8]&bitmask[i%8];
		bool const lower_bit = *crc&1;
		*crc >>= 1;
		if(lower_bit!=bit) *crc = (*crc^poly)|upper_mask;
	}
}

void crc_general(uint64_t *const crc, uint64_t poly, uint8_t const poly_width, uint8_t const *const data, uint8_t const offset, uint64_t const data_width, bool const lsb_first, bool const rshift)
{
	void (*crc_shift_byte)(uint64_t *const crc, uint64_t const poly, uint8_t const poly_width, uint8_t const *const data, uint64_t const data_width, bool const lsb_first);
	void (*crc_shift_bit)(uint64_t *const crc, uint64_t const poly, uint8_t const poly_width, uint8_t const *const data, uint8_t const offset, uint64_t const data_width, bool const lsb_first);
	
	if(rshift)
	{
		*crc &= generate_mask(poly_width);
		poly = reverse_bits(poly, poly_width);
		crc_shift_byte = crc_rshift_byte;
		crc_shift_bit  = crc_rshift_bit ;
	}
	else
	{
		crc_shift_byte = crc_lshift_byte;
		crc_shift_bit  = crc_lshift_bit ;
	}
	
	uint8_t const num_bits_pre = 7-(offset+7)%8;
	if(num_bits_pre>data_width || poly_width<8)
	{
		crc_shift_bit(crc, poly, poly_width, data, offset, data_width, lsb_first);
	}
	else
	{
		uint64_t const num_bytes = (data_width-num_bits_pre)/8;
		uint8_t const num_bits_post = data_width-num_bits_pre-num_bytes*8;
		
		crc_shift_bit (crc, poly, poly_width, data, offset     , num_bits_pre, lsb_first);
		crc_shift_byte(crc, poly, poly_width, data+(offset+7)/8, num_bytes   , lsb_first);
		
		if(num_bits_post)
			crc_shift_bit(crc, poly, poly_width, data+(offset+7)/8+num_bytes, 0, num_bits_post, lsb_first);
	}
	
	if(!rshift)
		*crc &= generate_mask(poly_width);
}

void crc_general_append(uint8_t *const data, uint8_t const offset, uint64_t crc, uint8_t const poly_width, bool const lsb_first, bool const rshift)
{
	if(offset)
	{
		char const *const bitmask = lsb_first?"\x01\x02\x04\x08\x10\x20\x40\x80":"\x80\x40\x20\x10\x08\x04\x02\x01";
		uint64_t const shiftmask = rshift?1:(uint64_t) 1<<(poly_width-1);
		
		for(uint8_t i=0; i<poly_width; i++)
		{
			uint16_t const idx = (uint16_t) offset+i;
			
			if(crc&shiftmask) data[idx/8] |=  bitmask[idx%8];
			else              data[idx/8] &= ~bitmask[idx%8];
			
			if(rshift) crc >>= 1;
			else       crc <<= 1;
		}
	}
	else
	{
		if(lsb_first^rshift) crc = reverse_bits(crc, poly_width);
		else crc &= generate_mask(poly_width);
		
		for(uint8_t i=0; i<(poly_width+7)/8; i++)
		{
			int8_t const nrshift = lsb_first?i*8:poly_width-8-i*8;
			
			if(nrshift<0) data[i] = crc<<-nrshift;
			else          data[i] = crc>> nrshift;
		}
	}
}

uint64_t crc_general_fetch(uint8_t const *const data, uint8_t const offset, uint8_t const poly_width, bool const lsb_first, bool const rshift)
{
	uint64_t crc = 0;
	
	if(offset)
	{
		char const *const bitmask = lsb_first?"\x01\x02\x04\x08\x10\x20\x40\x80":"\x80\x40\x20\x10\x08\x04\x02\x01";
		uint64_t const shiftmask = rshift?1:(uint64_t) 1<<(poly_width-1);
		
		for(uint8_t i=0; i<poly_width; i++)
		{
			uint16_t const idx = (uint16_t) offset+i;
			
			if(data[idx/8]&bitmask[idx%8]) crc |= shiftmask;
			
			if(rshift) crc <<= 1;
			else       crc >>= 1;
		}
	}
	else
	{
		for(uint8_t i=0; i<(poly_width+7)/8; i++)
		{
			int8_t const nlshift = lsb_first?i*8:poly_width-8-i*8;
			
			if(nlshift<0) crc |=              data[i]  >>-nlshift;
			else          crc |= ((uint64_t) (data[i]))<< nlshift;
		}
		
		if(lsb_first^rshift) crc = reverse_bits(crc, poly_width);
		else crc &= generate_mask(poly_width);
	}
	
	return crc;
}

uint8_t crc_general_compare(uint8_t const *const data, uint8_t const offset, uint64_t const crc, uint8_t const poly_width, bool const lsb_first, bool const rshift)
{
	uint64_t const tmp = crc_general_fetch(data, offset, poly_width, lsb_first, rshift);
	return (generate_mask(poly_width)&(crc^tmp))?1:0;
}


uint64_t crc_general_magic(uint64_t const poly, uint8_t const poly_width, bool const lsb_first, bool const rshift)
{
	uint64_t const data = -1;
	uint64_t crc = 0;
	crc_general(&crc, poly, poly_width, (uint8_t*) &data, 0, poly_width, lsb_first, rshift);
	return crc;
}

void crc_general_lut(uint64_t lut[CRC_LUT_LENGTH], uint64_t const poly, uint8_t const poly_width, bool const lsb_first, bool const rshift)
{
	uint8_t i = 0;
	do
	{
		lut[i] = 0;
		crc_general(lut+i, poly, poly_width, &i, 0, 8, lsb_first, rshift);
		i++;
	}
	while(i);
}

