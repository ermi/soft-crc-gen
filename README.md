# soft-crc-gen

soft-crc-gen is a code generator for CRC calculation. It is written in C and it also generates C code.
This software can handle CRC polynomials up to 64 bit and has multiple options to specify the CRC calculation.