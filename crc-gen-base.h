//  soft-crc-gen: CRC code generator
//  Copyright (C) 2022  ermi
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef crc_gen_base_h
#define crc_gen_base_h
#include "crc-utils.h"
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

struct generation_settings_t
{
	char *name;
	uint64_t seed, poly;
	uint8_t poly_width, sizebits;
	bool lsb_first, rshift, use_lut, bits;
};

uint8_t type_bits(uint8_t const width);
bool fulltype(uint8_t const width);
char *sizebits2str(uint8_t const width);

void fprint_hex(FILE *const stream, uint64_t const val, uint8_t const width);
void fprint_polynom(FILE *const stream, uint64_t const poly, uint8_t const width);

void fprint_head(FILE *const stream, struct generation_settings_t const *const settings);
void fprint_lut(FILE *const stream, uint64_t lut[CRC_LUT_LENGTH], uint8_t const poly_width);

void fprint_macro_value(FILE *const stream, uint64_t const x, uint8_t const poly_width);
void fprint_macro(FILE *const stream, char const *const name, char const *const subname, uint64_t const x, uint8_t const poly_width);

void soft_crc_gen_top_h(FILE *const stream, struct generation_settings_t const *const settings, char const *const basename);
void soft_crc_gen_bot_h(FILE *const stream);

void soft_crc_gen_top_c(FILE *const stream, struct generation_settings_t const *const settings, char const *const basename);

#endif

