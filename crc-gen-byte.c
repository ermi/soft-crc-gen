//  soft-crc-gen: CRC code generator
//  Copyright (C) 2022  ermi
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "crc-gen-byte.h"
#include <inttypes.h>

static inline void fprint_function_header_calc(FILE *const stream, char const *const name, char const *const suffix, uint8_t const poly_width, uint8_t const sizebits)
{
	fprintf(stream, "void %s_calc%s(uint%" PRIu8 "_t *const crc, uint8_t const *const data, %s const size)", name, suffix, type_bits(poly_width), sizebits2str(sizebits));
}

static inline void fprint_function_header_append(FILE *const stream, char const *const name, uint8_t const poly_width)
{
	fprintf(stream, "void %s_append(uint%" PRIu8 "_t const crc, uint8_t *const data)", name, type_bits(poly_width));
}

static inline void fprint_function_header_fetch(FILE *const stream, char const *const name, uint8_t const poly_width)
{
	fprintf(stream, "uint%" PRIu8 "_t %s_fetch(uint8_t const *const data)", type_bits(poly_width), name);
}

static inline void fprint_function_header_compare(FILE *const stream, char const *const name, uint8_t const poly_width)
{
	fprintf(stream, "uint8_t %s_compare(uint%" PRIu8 "_t const crc, uint8_t const *const data)", name, type_bits(poly_width));
}


void fprint_function_byte_calc(FILE *const stream, struct generation_settings_t const *const settings, bool const mask, char const *const suffix)
{
	uint8_t const tmp_shift = (settings->poly_width<8 && !settings->rshift && !settings->use_lut)?8-settings->poly_width:0;
	bool const reverse = settings->rshift^settings->lsb_first;
	
	fprint_function_header_calc(stream, settings->name, suffix, settings->poly_width, settings->sizebits);
	fputs("\n{\n", stream);
	if(mask && settings->rshift) fprintf(stream, "\t*crc &= 0x%" PRIx64 ";\n\t\n", generate_mask(settings->poly_width));
	
	if(tmp_shift) fprintf(stream, "\t*crc <<= %" PRIu8 ";\n\t\n", tmp_shift);
	
	fprintf(stream, "\tfor(%s i=0; i<size; i++)\n", sizebits2str(settings->sizebits));
	fputs("\t{\n", stream);
	
	if(settings->use_lut)
	{
		fputs("\t\t*crc = ", stream);
		if(settings->poly_width>8) fprintf(stream, "(*crc%s8)^", settings->rshift?">>":"<<");
		fputs("lut[", stream);
		if(reverse)
			fputs("reverse8(", stream);
		else
		{
			if(settings->poly_width>8) fputs("(uint8_t) ", stream);
			fputc('(', stream);
		}
		if(!settings->rshift && settings->poly_width!=8)
		{
			if(settings->poly_width<8)
				fprintf(stream, "*crc<<%" PRIu8, 8-settings->poly_width);
			else
				fprintf(stream, "*crc>>%" PRIu8, settings->poly_width-8);
		}
		else
			fputs("*crc", stream);
		fputs(")^", stream);
		fputs("data[i]", stream);
		fputs("];\n", stream);
	}
	else
	{
		bool const shift = (!settings->rshift) && (settings->poly_width>8);
		
		fputs("\t\t*crc ^= ", stream);
		if(shift) fprintf(stream, "((uint%" PRIu8 "_t) (", type_bits(settings->poly_width));
		fputs(reverse?"reverse8(data[i])":"data[i]", stream);
		if(shift) fprintf(stream, "))<<%u", settings->poly_width-8);
		fputs(";\n", stream);
		
		fputs("\t\tfor(uint8_t n=8; n; n--)\n", stream);
		fputs("\t\t{\n", stream);
		fputs("\t\t\tbool const tmp = *crc&", stream);
		if(settings->rshift) fputc('1', stream);
		else fprintf(stream, "0x%" PRIX64, (uint64_t) 1<<(settings->poly_width-1+tmp_shift));
		fputs(";\n", stream);
		fprintf(stream, "\t\t\t*crc %s= 1;\n", settings->rshift?">>":"<<");
		fputs("\t\t\tif(tmp) *crc ^= ", stream);
		fprint_macro_value(stream, settings->rshift?reverse_bits(settings->poly, settings->poly_width):settings->poly<<tmp_shift, settings->poly_width);
		fputs(";\n", stream);
		fputs("\t\t}\n", stream);
	}
	
	fputs("\t}\n", stream);
	if(tmp_shift) fprintf(stream, "\t\n\t*crc >>= %" PRIu8 ";\n", tmp_shift);
	
	if(mask && !settings->rshift) fprintf(stream, "\t\n\t*crc &= 0x%" PRIx64 ";\n", generate_mask(settings->poly_width));
	fputs("}\n\n", stream);
}

static inline void fprint_function_byte_append(FILE *const stream, struct generation_settings_t const *const settings)
{
	bool const reverse = settings->rshift^settings->lsb_first;
	
	fprint_function_header_append(stream, settings->name, settings->poly_width);
	fputs("\n{\n", stream);
	
	for(uint8_t i=0; i<(settings->poly_width+7)/8; i++)
	{
		int8_t const nrshift = settings->rshift?8*i:settings->poly_width-8*(1+i);
		
		fprintf(stream, "\tdata[%" PRIu8 "] = ", i);
		if(reverse) fputs("reverse8(", stream);
		if(nrshift<0) fprintf(stream, "crc<<%" PRIi8, -nrshift);
		else          fprintf(stream, "crc>>%" PRIi8,  nrshift);
		if(reverse) fputc(')', stream);
		fputs(";\n", stream);
	}
	
	fputs("}\n\n", stream);
}

static inline void fprint_function_byte_fetch(FILE *const stream, struct generation_settings_t const *const settings)
{
	bool const reverse = settings->rshift^settings->lsb_first;
	
	fprint_function_header_fetch(stream, settings->name, settings->poly_width);
	fputs("\n{\n", stream);
	
	fprintf(stream, "\tuint%" PRIu8 "_t crc = 0;\n", type_bits(settings->poly_width));
	
	for(uint8_t i=0; i<(settings->poly_width+7)/8; i++)
	{
		int8_t const nlshift = settings->rshift?8*i:settings->poly_width-8*(1+i);
		
		fprintf(stream, "\tcrc |= (uint%" PRIu8 "_t) ", type_bits(settings->poly_width));
		if(reverse) fputs("reverse8(", stream);
		fprintf(stream, "data[%" PRIu8 "]", i);
		if(reverse) fputc(')', stream);
		if(nlshift<0) fprintf(stream, ">>%" PRIi8, -nlshift);
		else          fprintf(stream, "<<%" PRIi8,  nlshift);
		fputs(";\n", stream);
	}
	
	fputs("\treturn crc", stream);
	if(settings->rshift && !fulltype(settings->poly_width))
		fprintf(stream, "&0x%" PRIx64, generate_mask(settings->poly_width));
	fputs(";\n", stream);
	
	fputs("}\n\n", stream);
}

static inline void fprint_function_byte_compare(FILE *const stream, struct generation_settings_t const *const settings)
{
	bool const mask = !fulltype(settings->poly_width);
	
	fprint_function_header_compare(stream, settings->name, settings->poly_width);
	fputs("\n{\n", stream);
	
	fprintf(stream, "\tuint%" PRIu8 "_t const tmp = %s_fetch(data);\n", type_bits(settings->poly_width), settings->name);
	
	fputs("\treturn ", stream);
	if(mask) fprintf(stream, "(0x%" PRIx64 "&", generate_mask(settings->poly_width));
	fputs("(crc^tmp)", stream);
	if(mask) fputc(')', stream);
	fputs("?1:0;\n", stream);
	
	fputs("}\n\n", stream);
}


void soft_crc_gen_byte_h(FILE *const stream, struct generation_settings_t const *const settings, char const *const basename)
{
	soft_crc_gen_top_h(stream, settings, basename);
	
	fprint_function_header_calc(stream, settings->name, "", settings->poly_width, settings->sizebits);
	fputs(";\n", stream);
	fprint_function_header_append(stream, settings->name, settings->poly_width);
	fputs(";\n", stream);
	fprint_function_header_fetch(stream, settings->name, settings->poly_width);
	fputs(";\n", stream);
	fprint_function_header_compare(stream, settings->name, settings->poly_width);
	fputs(";\n", stream);
	
	soft_crc_gen_bot_h(stream);
}

void soft_crc_gen_byte_c(FILE *const stream, struct generation_settings_t const *const settings, char const *const basename)
{
	bool const mask = !fulltype(settings->poly_width);
	
	soft_crc_gen_top_c(stream, settings, basename);
	
	fprint_function_byte_calc   (stream, settings, mask, "");
	fprint_function_byte_append (stream, settings);
	fprint_function_byte_fetch  (stream, settings);
	fprint_function_byte_compare(stream, settings);
}

