//  soft-crc-gen: CRC code generator
//  Copyright (C) 2022  ermi
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef crc_utils_h
#define crc_utils_h
#include <stdint.h>
#include <stdbool.h>

#define CRC_LUT_LENGTH 256

uint64_t reverse_bits(uint64_t const x, uint8_t const width);
uint64_t generate_mask(uint8_t const width);

void crc_general(uint64_t *const crc, uint64_t const poly, uint8_t const poly_width, uint8_t const *const data, uint8_t const offset, uint64_t const data_width, bool const lsb_first, bool const rshift);

void crc_general_append(uint8_t *const data, uint8_t const offset, uint64_t const crc, uint8_t const poly_width, bool const lsb_first, bool const rshift);
uint64_t crc_general_fetch(uint8_t const *const data, uint8_t const offset, uint8_t const poly_width, bool const lsb_first, bool const rshift);
uint8_t crc_general_compare(uint8_t const *const data, uint8_t const offset, uint64_t const crc, uint8_t const poly_width, bool const lsb_first, bool const rshift);

uint64_t crc_general_magic(uint64_t const poly, uint8_t const poly_width, bool const lsb_first, bool const rshift);
void crc_general_lut(uint64_t lut[CRC_LUT_LENGTH], uint64_t const poly, uint8_t const poly_width, bool const lsb_first, bool const rshift);

#endif

