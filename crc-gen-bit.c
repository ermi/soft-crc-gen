//  soft-crc-gen: CRC code generator
//  Copyright (C) 2022  ermi
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "crc-gen-bit.h"
#include "crc-gen-byte.h"
#include <inttypes.h>

static inline void fprint_function_header_calc(FILE *const stream, char const *const name, char const *const suffix, uint8_t const poly_width, uint8_t const sizebits)
{
	fprintf(stream, "void %s_calc%s(uint%" PRIu8 "_t *const crc, uint8_t const *const data, uint8_t const offset, %s const size)", name, suffix, type_bits(poly_width), sizebits2str(sizebits));
}

static inline void fprint_function_header_append(FILE *const stream, char const *const name, uint8_t const poly_width)
{
	fprintf(stream, "void %s_append(uint%" PRIu8 "_t crc, uint8_t *const data, uint8_t const offset)", name, type_bits(poly_width));
}

static inline void fprint_function_header_fetch(FILE *const stream, char const *const name, uint8_t const poly_width)
{
	fprintf(stream, "uint%" PRIu8 "_t %s_fetch(uint8_t *const data, uint8_t const offset)", type_bits(poly_width), name);
}

static inline void fprint_function_header_compare(FILE *const stream, char const *const name, uint8_t const poly_width)
{
	fprintf(stream, "uint8_t %s_compare(uint%" PRIu8 "_t crc, uint8_t *const data, uint8_t const offset)", name, type_bits(poly_width));
}


static inline void fprint_function_bit_calc(FILE *const stream, struct generation_settings_t const *const settings, bool const mask, char const *const suffix)
{
	fprint_function_header_calc(stream, settings->name, suffix, settings->poly_width, settings->sizebits);
	fputs("\n{\n", stream);
	if(mask && settings->rshift) fprintf(stream, "\t*crc &= 0x%" PRIx64 ";\n\t\n", generate_mask(settings->poly_width));
	
	fprintf(stream, "\t%s const subsequent = offset+size;\n\t\n", sizebits2str(settings->sizebits));
	fprintf(stream, "\tfor(%s i=offset; i<subsequent; i++)\n", sizebits2str(settings->sizebits));
	fputs("\t{\n", stream);
	fputs("\t\tbool const bit = data[i/8]&BITMASK[i%8];\n", stream);
	fputs("\t\tbool const tmp = *crc&", stream);
	fprint_macro_value(stream, settings->rshift?1:(uint64_t) 1<<(settings->poly_width-1), settings->poly_width);
	fputs(";\n", stream);
	fprintf(stream, "\t\t*crc %s= 1;\n", settings->rshift?">>":"<<");
	fputs("\t\tif(tmp^bit) *crc = (*crc^", stream);
	fprint_macro_value(stream, settings->rshift?reverse_bits(settings->poly, settings->poly_width):settings->poly, settings->poly_width);
	fputs(")|", stream);
	fprint_macro_value(stream, settings->rshift?(uint64_t) 1<<(settings->poly_width-1):1, settings->poly_width);
	fputs(";\n", stream);
	fputs("\t}\n", stream);
	
	if(mask && !settings->rshift) fprintf(stream, "\t\n\t*crc &= 0x%" PRIx64 ";\n", generate_mask(settings->poly_width));
	fputs("}\n\n", stream);
}

static inline void fprint_function_bit_append(FILE *const stream, struct generation_settings_t const *const settings)
{
	fprint_function_header_append(stream, settings->name, settings->poly_width);
	fputs("\n{\n", stream);
	
	fprintf(stream, "\tfor(uint8_t i=0; i<%" PRIu8 "; i++)\n", settings->poly_width);
	fputs("\t{\n", stream);
	fputs("\t\tuint16_t const idx = (uint16_t) offset+i;\n", stream);
	fputs("\t\t\n", stream);
	
	fputs("\t\tif(crc&", stream);
	fprint_macro_value(stream, settings->rshift?1:(uint64_t) 1<<(settings->poly_width-1), settings->poly_width);
	fputs(") ", stream);
	fprintf(stream, "data[idx/8] |=  BITMASK[idx%%8];\n");
	fputs("\t\telse       ", stream);
	for(uint8_t i=(settings->poly_width+3)/4; i>0; i--) fputc(' ', stream);
	fprintf(stream, "data[idx/8] &= ~BITMASK[idx%%8];\n");
	
	fputs("\t\t\n", stream);
	fprintf(stream, "\t\tcrc %s= 1;\n", settings->rshift?">>":"<<");
	fputs("\t}\n", stream);
	fputs("}\n\n", stream);
}

static inline void fprint_function_bit_fetch(FILE *const stream, struct generation_settings_t const *const settings)
{
	fprint_function_header_fetch(stream, settings->name, settings->poly_width);
	fputs("\n{\n", stream);
	
	fprintf(stream, "\tuint%" PRIu8 "_t crc = 0;\n\t\n", type_bits(settings->poly_width));
	
	fprintf(stream, "\tfor(uint8_t i=0; i<%" PRIu8 "; i++)\n", settings->poly_width);
	fputs("\t{\n", stream);
	fputs("\t\tuint16_t const idx = (uint16_t) offset+i;\n", stream);
	fputs("\t\t\n", stream);
	
	fputs("\t\tif(data[idx/8]&BITMASK[idx%8]) crc |= ", stream);
	fprint_macro_value(stream, settings->rshift?1:(uint64_t) 1<<(settings->poly_width-1), settings->poly_width);
	fputs(";\n", stream);
	
	fputs("\t\t\n", stream);
	fprintf(stream, "\t\tcrc %s= 1;\n", settings->rshift?"<<":">>");
	fputs("\t}\n", stream);
	
	fputs("\t\n\treturn crc;\n", stream);
	fputs("}\n\n", stream);
}

static inline void fprint_function_bit_compare(FILE *const stream, struct generation_settings_t const *const settings)
{
	bool const mask = !fulltype(settings->poly_width);
	
	fprint_function_header_compare(stream, settings->name, settings->poly_width);
	fputs("\n{\n", stream);
	
	fprintf(stream, "\tuint%" PRIu8 "_t const tmp = %s_fetch(data, offset);\n", type_bits(settings->poly_width), settings->name);
	
	fputs("\treturn ", stream);
	if(mask) fprintf(stream, "(0x%" PRIx64 "&", generate_mask(settings->poly_width));
	fputs("(crc^tmp)", stream);
	if(mask) fputc(')', stream);
	fputs("?1:0;\n", stream);
	
	fputs("}\n\n", stream);
}


static inline void fprint_function_calc(FILE *const stream, struct generation_settings_t const *const settings, bool const mask)
{
	fprint_function_header_calc(stream, settings->name, "", settings->poly_width, settings->sizebits);
	fputs("\n{\n", stream);
	if(mask && settings->rshift) fprintf(stream, "\t*crc &= 0x%" PRIx64 ";\n\t\n", generate_mask(settings->poly_width));
	
	fputs("\tuint8_t const num_bits_pre = 7-(offset+7)%8;\n", stream);
	fputs("\t\n", stream);
	
	fputs("\tif(num_bits_pre>size)\n", stream);
	fprintf(stream, "\t\t%s_calc_bit(crc, data, offset, size);\n", settings->name);
	fputs("\telse\n", stream);
	fputs("\t{\n", stream);
	
	fputs("\t\tuint64_t const num_bytes = (size-num_bits_pre)/8;\n", stream);
	fputs("\t\tuint8_t const num_bits_post = size-num_bits_pre-num_bytes*8;\n", stream);
	fputs("\t\t\n", stream);
	
	fprintf(stream, "\t\t%s_calc_bit (crc, data, offset     , num_bits_pre);\n", settings->name);
	fprintf(stream, "\t\t%s_calc_byte(crc, data+(offset+7)/8, num_bytes   );\n", settings->name);
	fputs("\t\tif(num_bits_post)\n", stream);
	fprintf(stream, "\t\t\t%s_calc_bit(crc, data+(offset+7)/8+num_bytes, 0, num_bits_post);\n", settings->name);
	
	fputs("\t}\n", stream);
	
	if(mask && !settings->rshift) fprintf(stream, "\t\n\t*crc &= 0x%" PRIx64 ";\n", generate_mask(settings->poly_width));
	fputs("}\n\n", stream);
}


void soft_crc_gen_bit_h(FILE *const stream, struct generation_settings_t const *const settings, char const *const basename)
{
	soft_crc_gen_top_h(stream, settings, basename);
	
	fprint_function_header_calc(stream, settings->name, "", settings->poly_width, settings->sizebits);
	fputs(";\n", stream);
	fprint_function_header_append(stream, settings->name, settings->poly_width);
	fputs(";\n", stream);
	fprint_function_header_fetch(stream, settings->name, settings->poly_width);
	fputs(";\n", stream);
	fprint_function_header_compare(stream, settings->name, settings->poly_width);
	fputs(";\n", stream);
	
	soft_crc_gen_bot_h(stream);
}

void soft_crc_gen_bit_c(FILE *const stream, struct generation_settings_t const *const settings, char const *const basename)
{
	bool const mask = !fulltype(settings->poly_width);
	
	soft_crc_gen_top_c(stream, settings, basename);
	
	if(settings->poly_width<8)
	{
		fprint_function_bit_calc(stream, settings, mask, "");
	}
	else
	{
		fputs("static inline ", stream);
		fprint_function_bit_calc (stream, settings, mask, "_bit" );
		fputs("static inline ", stream);
		fprint_function_byte_calc(stream, settings, mask, "_byte");
		fprint_function_calc     (stream, settings, mask);
	}
	
	fprint_function_bit_append (stream, settings);
	fprint_function_bit_fetch  (stream, settings);
	fprint_function_bit_compare(stream, settings);
}

