//  soft-crc-gen: CRC code generator
//  Copyright (C) 2022  ermi
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "crc-utils.h"
#include "crc-gen.h"
#include "poly_parser.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>
#include <limits.h>
#include <argp.h>
#include <assert.h>


struct args_t
{
	char **files;
	int num_files;
	struct generation_settings_t settings;
	bool pipe;
};


static error_t parse_opt(int key, char *arg, struct argp_state *state)
{
	struct args_t *const args = state->input;
	char *ptr;
	uintmax_t tmp;
	
	switch(key)
	{
	case 's':
		if(!strcmp(arg, "zeros"))
		{
			args->settings.seed = 0;
			break;
		}
		else if(!strcmp(arg, "ones"))
		{
			args->settings.seed = UINT64_MAX;
			break;
		}
		tmp = strtoumax(arg, &ptr, 16);
		switch(tmp)
		{
		case 0:
			if(ptr!=arg && *ptr==0) break;
			__attribute__((fallthrough));
		case UINTMAX_MAX:
			argp_error(state, "invalid seed value (%s)\n", arg);
		}
		if(tmp>UINT64_MAX)
			argp_error(state, "invalid seed value (%s)\n", arg);
		args->settings.seed = tmp;
		break;
	
	case -'t':
		tmp = strtoumax(arg, &ptr, 10);
		switch(tmp)
		{
		case 0:
			if(ptr!=arg && *ptr==0) break;
			__attribute__((fallthrough));
		case UINTMAX_MAX:
			argp_error(state, "invalid sizebits value (%s)\n", arg);
		}
		if(tmp>64)
			argp_error(state, "invalid sizebits value (%s)\n", arg);
		args->settings.sizebits = tmp?type_bits(tmp):0;
		if(args->settings.sizebits!=tmp) fprintf(stderr, "Warning: increase sizebits from %s to %" PRIu8, arg, args->settings.sizebits);
		break;
	
	case 'n':
		args->settings.name = arg;
		break;
	
	case 'l':
		args->settings.lsb_first = true;
		break;
	
	case 'r':
		args->settings.rshift = true;
		break;
	
	case 't':
		args->settings.use_lut = true;
		break;
	
	case 'p':
		args->pipe = true;
		break;
	
	case 'b':
		args->settings.bits = true;
		break;
	
	case ARGP_KEY_ARG:
		switch(state->arg_num)
		{
		case 0:
			if(strchr(arg, '/'))
			{
				tmp = strtoumax(arg, &ptr, 16);
				switch(tmp)
				{
				case 0:
					if(ptr!=arg && *ptr=='/') break;
					__attribute__((fallthrough));
				case UINTMAX_MAX:
					argp_error(state, "invalid polynom (%s)\n", arg);
				}
				if(tmp>UINT64_MAX)
					argp_error(state, "invalid polynom (%s)\n", arg);
				args->settings.poly = tmp;
				
				tmp = strtoumax(++ptr, &ptr, 10);
				if(tmp<=0 || tmp>64) argp_error(state, "invalid polynom (%s)\n", arg);
				args->settings.poly_width = tmp;
			}
			else
			{
				size_t pos;
				int8_t const ret = poly_parse_str(&pos, &args->settings.poly, &args->settings.poly_width, arg);
				if(ret<0) argp_error(state, "invalid polynom string (%s)\n", arg);
			}
			break;
		default:
			args->files = reallocarray(args->files, args->num_files+1, sizeof(char*));
			if(!args->files) argp_error(state, "reallocarray: %s\n", strerror(errno));
			args->files[args->num_files++] = arg;
			break;
		}
		break;
	
	case ARGP_KEY_END:
		if(!args->settings.poly) argp_usage(state); // Not enough args.
		break;
	
	default:
		return ARGP_ERR_UNKNOWN;
	}
	return 0;
}

/********/
/* main */
/********/
int main(int argc, char **argv)
{
	struct args_t args;
	memset(&args, 0, sizeof(args));
	
	static char const doc[] = "\033[1A\vparameters:\n"
		"  POLY_VAL/POLY_WIDTH: polynomial value in hex and width {1..64}\n"
		"  POLY_STRING        : polynomial string\n"
		"  FILENAME           : output filename\n"
		"\nexample: J1850 crc = x^8+x^4+x^3+x^2+1\n"
		"  soft-crc-gen --right --lsb_first --seed ones --table 1D/8 crc8.c crc8.h\n"
		"  soft-crc-gen --right --lsb_first --seed ones x^8+x^4+x^3+x^2+1 crc8.c crc8.h\n";
	static char const args_doc[] = "<POLY_VAL/POLY_WIDTH|POLY_STRING> [FILENAME]";
	static struct argp_option const options[] = {
		{ "name"     ,  'n', "NAME", 0, "Set library name (default: crc)", 0 },
		{ "seed"     ,  's', "VAL" , 0, "Set seed value {HEX-STRING|low|high|zeros|ones} (default: zeros)", 0 },
		{ "sizebits" , -'t', "BITS", 0, "Size of size variable type", 0 },
		{ "lsb_first",  'l', NULL  , 0, "Use lsb first (default: msb first)", 0 },
		{ "right"    ,  'r', NULL  , 0, "Right shifting CRC (default: left shifting)", 0 },
		{ "table"    ,  't', NULL  , 0, "Generate crc with lookuptable", 0 },
		{ "pipe"     ,  'p', NULL  , 0, "Write to standart output (default: write to file)", 0 },
		{ "bits"     ,  'b', NULL  , 0, "Generate bit by bit functions", 0 },
		{ 0 }
	};
	static struct argp const argp = { options, parse_opt, args_doc, doc, 0, 0, 0 };
	
	argp_parse(&argp, argc, argv, 0, 0, &args);
	
	printf("name       = %s\n", args.settings.name      );
	printf("sizebits   = %u\n", args.settings.sizebits  );
	printf("poly_width = %u\n", args.settings.poly_width);
	
	fputs("poly       = 0x", stdout);
	fprint_hex(stdout, args.settings.poly, args.settings.poly_width);
	putchar('\n');
	fputs("seed       = 0x", stdout);
	fprint_hex(stdout, args.settings.seed, args.settings.poly_width);
	putchar('\n');
	
	printf("lsb_first  = %s\n", args.settings.lsb_first?"true":"false");
	printf("rshift     = %s\n", args.settings.rshift   ?"true":"false");
	printf("pipe       = %s\n", args.pipe              ?"true":"false");
	printf("bits       = %s\n", args.settings.bits     ?"true":"false");
	
	char name[8];
	if(!args.settings.name)
	{
		snprintf(name, sizeof(name), "crc%" PRIu8, args.settings.poly_width);
		args.settings.name = name;
	}
	
	if(args.num_files)
	{
		fputs("files:", stdout);
		for(int i=0; i<args.num_files; i++) printf(" %s", args.files[i]);
		putchar('\n');
		
		for(int i=0; i<args.num_files; i++)
		{
			if(args.pipe) fputs("<<<<<<<<\n", stdout);
			if(soft_crc_gen(&args.settings, args.files[i], args.pipe)==-1) return 1;
			if(args.pipe) fputs(">>>>>>>>\n", stdout);
		}
	}
	else
		if(soft_crc_gen(&args.settings, NULL, args.pipe)==-1) return 1;
	
	return 0;
}

