//  soft-crc-gen: CRC code generator
//  Copyright (C) 2022  ermi
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "poly_parser.h"
#include <string.h>
#include <ctype.h>

static inline char poly_lexer(uint8_t *const val, char const **const str)
{
	char c = *(*str)++;
	
	if(c==0) return 0;
	if(strchr("+x^", c)) return c;
	if(isdigit(c))
	{
		uint8_t x = c-'0';
		while(isdigit(**str))
		{
			if(!x) return POLY_PARSER_INVALID_TOKEN;
			c = *(*str)++;
			if(x>25) return POLY_PARSER_INT_OVERFLOW;
			x *= 10;
			uint8_t const y = x+c-'0';
			if(y<x) return POLY_PARSER_INT_OVERFLOW;
			x = y;
		}
		if(val) *val = x;
		return '0';
	}
	if(isspace(c))
	{
		while(isspace(**str)) (*str)++;
		return ' ';
	}
	return POLY_PARSER_INVALID_TOKEN;
}

int8_t poly_parse_str(size_t *const pos, uint64_t *const poly, uint8_t *const width, char const *const str)
{
	char const *ptr = str;
	uint64_t lpoly = 0;
	uint8_t bit64 = 0;
	uint8_t lwidth = 0;
	uint8_t exp = 0;
	char c = '+';
	
	while(1)
	{
		char const r = poly_lexer(&exp, &ptr);
		if(pos) *pos = ptr-str-1;
		switch(r)
		{
		case 0:
		case '+':
			if(c=='x')
			{
				if(lpoly&2) return POLY_PARSER_DOUBLED_EXP;
				lpoly |= 2;
				if(lwidth<1) lwidth = 1;
			}
			else if(c!='0') return POLY_PARSER_WRONG_ORDER;
			if(!r)
			{
				if(!strchr("x0", c)) return POLY_PARSER_WRONG_ORDER;
				lpoly &= ~((uint64_t) 1<<lwidth);
				if(poly ) *poly  = lpoly ;
				if(width) *width = lwidth;
				return 0;
			}
			c = '+';
			break;
		case 'x':
			if(c!='+') return POLY_PARSER_WRONG_ORDER;
			c = 'x';
			break;
		case '^':
			if(c!='x') return POLY_PARSER_WRONG_ORDER;
			c = '^';
			break;
		case '0':
			if(c=='^')
			{
				if(exp>64)
					return POLY_PARSER_INT_OVERFLOW;
				else if(exp==64)
				{
					if(bit64) return POLY_PARSER_DOUBLED_EXP;
					bit64 = 1;
					if(lwidth<64) lwidth = 64;
				}
				else
				{
					uint64_t const mask = (uint64_t) 1<<exp;
					if(lpoly&mask) return POLY_PARSER_DOUBLED_EXP;
					lpoly |= mask;
					if(lwidth<exp) lwidth = exp;
				}
			}
			else if(c=='+' && exp==1)
			{
				if(lpoly&1) return POLY_PARSER_DOUBLED_EXP;
				lpoly |= 1;
			}
			else
				return POLY_PARSER_WRONG_ORDER;
			c = '0';
			break;
		case ' ':
			break;
		default:
			return r;
		}
	}
}

