#  soft-crc-gen: CRC code generator
#  Copyright (C) 2022  ermi
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

CC := gcc
OBJ = $(addprefix $(BUILD_DIR)/,$(addsuffix .o,$(basename $(SRC))))
BUILD_DIR := build
MARKER := $(BUILD_DIR)/.marker
DST := soft-crc-gen

SRC := main.c
SRC += crc-utils.c
SRC += crc-gen.c
SRC += crc-gen-base.c
SRC += crc-gen-byte.c
SRC += crc-gen-bit.c
SRC += poly_parser.c

#########
# FLAGS #
#########
FLAGS := -Wall -Wextra -Wpedantic -Wshadow

CFLAGS := $(FLAGS)
CFLAGS += -MMD
CFLAGS += -fpie
CFLAGS += -fstack-protector-strong
ifdef DEBUG
CFLAGS += -ggdb
CFLAGS += -g$(DEBUG)
CFLAGS += -O0
else ifdef OPTIMISE
CFLAGS += -O$(OPTIMISE)
else
CFLAGS += -O2
endif

LDFLAGS := $(FLAGS)

###############
# BUILD RULES #
###############
all: $(BUILD_DIR)/$(DST)

$(BUILD_DIR)/$(DST): $(OBJ)
	$(CC) $^ -o $@ $(LDFLAGS)

$(BUILD_DIR)/%.o: %.c $(MARKER)
	$(CC) -c $< -o $@ $(CFLAGS)

$(MARKER):
	mkdir -p $(BUILD_DIR)
	touch $@

-include $(OBJ:.o=.d)


.PHONY: all clean

clean:
	rm -rvf $(BUILD_DIR)

