//  soft-crc-gen: CRC code generator
//  Copyright (C) 2022  ermi
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "crc-gen.h"
#include "crc-gen-base.h"
#include "crc-gen-byte.h"
#include "crc-gen-bit.h"
#include <stdlib.h>
#include <string.h>

static inline void soft_crc_gen_h(FILE *const stream, struct generation_settings_t const *const settings, char const *const basename)
{
	if(settings->bits) soft_crc_gen_bit_h (stream, settings, basename);
	else               soft_crc_gen_byte_h(stream, settings, basename);
}

static inline void soft_crc_gen_c(FILE *const stream, struct generation_settings_t const *const settings, char const *const basename)
{
	if(settings->bits) soft_crc_gen_bit_c (stream, settings, basename);
	else               soft_crc_gen_byte_c(stream, settings, basename);
}

static inline int soft_crc_gen_lut_only(FILE *const stream, struct generation_settings_t const *const settings)
{
	uint64_t lut[CRC_LUT_LENGTH];
	crc_general_lut(lut, settings->poly, settings->poly_width, settings->lsb_first, settings->rshift);
	
	fprint_lut(stream, lut, settings->poly_width);
	
	return 0;
}

int soft_crc_gen(struct generation_settings_t const *const settings, char const *const fname, bool const pipe)
{
	if(fname)
	{
		FILE *stream = stdout;
		char *basename = strrchr(fname, '/');
		basename = strdup(basename?basename+1:fname);
		if(!basename)
		{
			perror("soft_crc_gen");
			return -1;
		}
		if(!*basename)
		{
			fprintf(stderr, "soft_crc_gen: no valid filename (%s)\n", fname);
			free(basename);
			return -1;
		}
		
		char *extension = strrchr(basename, '.');
		if(!extension)
		{
			fprintf(stderr, "soft_crc_gen: no valid filename (%s)\n", fname);
			free(basename);
			return -1;
		}
		*extension++ = 0;
		if(strlen(extension)!=1)
		{
			fprintf(stderr, "soft_crc_gen: no valid filename (%s)\n", fname);
			free(basename);
			return -1;
		}
		
		if(!pipe)
		{
			stream = fopen(fname, "w");
			if(!stream)
			{
				perror("fopen");
				free(basename);
				return -1;
			}
		}
		
		switch(*extension)
		{
		case 'h': soft_crc_gen_h(stream, settings, basename); break;
		case 'c': soft_crc_gen_c(stream, settings, basename); break;
		default:
			fprintf(stderr, "soft_crc_gen: no valid filename (%s)\n", fname);
			free(basename);
			return -1;
		}
		
		if(!pipe && fclose(stream)==-1)
		{
			perror("soft_crc_gen");
			free(basename);
			return -1;
		}
		free(basename);
		return 0;
	}
	else
	{
		return soft_crc_gen_lut_only(stdout, settings);
	}
}

